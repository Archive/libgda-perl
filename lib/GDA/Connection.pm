#!/usr/bin/perl -w
#
# Connection.pm
#
# $Revision$
#
# NOTE:
#
#   * gda_connection_add_errorlist() is not used. Perl looping and
#     multiple calls to gda_connection_add_single_error() are used
#     instead.
#
# TODO:
#
#   * gda_connection_corba_exception()
#   * gda_connection_create_recordset()
#   * gda_connection_get_cursor_location()
#   * gda_connection_modify_schema()
#   * gda_connection_open_schema()
#   * gda_connection_open_schema_array()
#   * gda_connection_set_cursor_location()
#   * What about the 'error', 'open', 'warning' and 'close' signals?
#   * What about the following functions that appear to be in libgda-client.a
#     (based on running 'strings libgda-client.a'):
#
#       * gda_connection_class_init()
#       * gda_connection_destroy()
#       * gda_connection_real_error()
#       * gda_connection_real_warning()
#       * gda_connection_signals()
#
# Copyright (C) 2001 Gregor N. Purdy. All rights reserved.
#
# This program is free software. It may be modified and/or
# distributed under the same terms as Perl itself.
# 


use strict;

package GDA::Connection;

use GDA;

use Inline 'C';

use Inline 'C' => Config =>
  INC          => '-I/usr/include/gda -I/usr/include/glib-1.2 -I/usr/lib/glib/include -I/usr/include/gtk-1.2 -I/usr/include/gnome-xml',
  AUTO_INCLUDE => '#include "gda-connection.h"',
  LIBS         => '-lgda-client -lgda-common';
Inline->init; # TODO: Why is this necessary?


#
# WRAPPERS:
#

sub new
{
  my $class = shift;
  my %options = @_;

  my $self = new_impl($class);

  my ($provider, $connect, $conn_to, $comm_to, $user, $password, $flags);

  $provider = $options{PROVIDER}        if exists $options{PROVIDER};
  $connect  = $options{CONNECT}         if exists $options{CONNECT};
  $conn_to  = $options{CONNECT_TIMEOUT} if exists $options{CONNECT_TIMEOUT};
  $comm_to  = $options{COMMAND_TIMEOUT} if exists $options{COMMAND_TIMEOUT};
  $user     = $options{USER}            if exists $options{USER};
  $password = $options{PASSWORD}        if exists $options{PASSWORD};
  $flags    = $options{FLAGS}           if exists $options{FLAGS};

  $self->provider($provider) if $provider;

  $self->connect_timeout($conn_to) if defined $conn_to;
  $self->command_timeout($comm_to) if defined $comm_to;
  $self->flags($flags)             if defined $flags;

  if ($connect) {
    print STDERR "GDA::Connection: Automatically connecting...\n";

    $self->provider('Default') unless $provider; # TODO: Necessary?

    my $result = $self->open($connect, $user, $password);

    warn "GDA::Connection::new(): Automatic open of connection failed!"
      unless $result == 0;
  }

  return $self;
}

sub close
{
  my $self = shift;
  return $self->close_impl(@_);
}

sub command_timeout
{
  my $self = shift;

  if (@_) {
    $self->set_command_timeout(shift);
  } else {
    return $self->get_command_timeout();
  }
}

sub connect_timeout
{
  my $self = shift;

  if (@_) {
    $self->set_connect_timeout(shift);
  } else {
    return $self->get_connect_timeout();
  }
}

sub error
{
  my $self = shift;

  if (@_) {
    foreach my $error (@_) { $self->add_error($error); }
  } else {
    return $self->get_errors();
  }
}

sub flags
{
  my $self = shift;

  if (@_) {
    $self->set_flags(shift);
  } else {
    return $self->get_flags();
  }
}

sub open
{
  my $self = shift;
  my ($connect, $user, $password) = @_;

  $connect = '' unless defined $connect;

  if (ref $connect eq 'HASH') { # Map hash connect to string connect
    print STDERR "GDA::Connection: Converting HASH connect to string...\n";
    $connect =
      join("; ",
        map({ sprintf("%s:%s", $_, $connect->{$_}) }
          sort({ $a cmp $b }
            keys %$connect)));
    print STDERR "  ...$connect\n";
  }

  $user     = '' unless defined $user;
  $password = '' unless defined $password;

  return $self->open_impl($connect, $user, $password);
}

sub provider
{
  my $self = shift;

  if (@_) {
    my $provider = shift;

    #
    # If provider isn't of the form:
    #
    #   OAFIID:GNOME_GDA_Provider_*_ConnectionFactory
    #
    # then fix it up so that it is.
    #

    $provider = sprintf("GNOME_GDA_Provider_%s", $provider)
      unless $provider =~ /GNOME_GDA_Provider_/;
    $provider = sprintf("OAFIID:%s", $provider)
      unless $provider =~ /^OAFIID:/;
    $provider = sprintf("%s_ConnectionFactory", $provider)
      unless $provider =~ /_ConnectionFactory$/;

    print STDERR "GDA::Connection: Setting provider to '$provider'.\n";
    $self->set_provider($provider);
  } else {
    return $self->get_provider();
  }
}

1;

=head1 NAME

GDA::Connection - GDA connection Perl bindings

=head1 SYNOPSIS

  use GDA 'my_app', 'my_ver', 'my_prog';
  use GDA::Connection;
  ...

=head1 DESCRIPTION

TODO

=head1 AUTHOR

Gregor N. Purdy E<lt>gregor@focusresearch.comE<gt>

=head1 LICENSE

This program is free software. It may be modified and/or
distributed under the same terms as Perl itself.

=head1 COPYRIGHT

Copyright (C) 2001 Gregor N. Purdy. All rights reserved.

=cut

__DATA__
__C__


/*********************************************************************
** SUPPORT                                                          **
*********************************************************************/

static SV * objectify_(char * class, void * self)
{
  SV * obj_ref;
  SV * obj;

  obj_ref = newSViv(0);
  obj     = newSVrv(obj_ref, class);

  sv_setiv(obj, (IV)self);
  SvREADONLY_on(obj);

  return obj_ref;
}


/*********************************************************************
** CONSTRUCTOR and DESTRUCTOR                                       **
*********************************************************************/

SV * new_impl(char * class)
{
  GdaConnection * self;
  CORBA_ORB       orb;

  orb  = (CORBA_ORB)gda_corba_get_orb();
  self = gda_connection_new(orb);

  return objectify_(class, self);
}

void DESTROY(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  gda_connection_free(self);
}


/*********************************************************************
** ATTRIBUTE: Command Timeout                                       **
*********************************************************************/

long get_command_timeout(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_get_cmd_timeout(self);
}

void set_command_timeout(SV * obj, long timeout)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  gda_connection_set_cmd_timeout(self, timeout);
}


/*********************************************************************
** ATTRIBUTE: Connection Timeout                                    **
*********************************************************************/

long get_connect_timeout(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_get_connect_timeout(self);
}

void set_connect_timeout(SV * obj, long timeout)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  gda_connection_set_connect_timeout(self, timeout);
}


/*********************************************************************
** ATTRIBUTE: DSN                                                   **
*********************************************************************/

char * dsn(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_get_dsn(self);
}


/*********************************************************************
** ATTRIBUTE: Errors                                                **
*********************************************************************/

void add_error(SV * obj, SV * err_obj)
{
  GdaConnection * self;
  GdaError *      error;

  self   = (GdaConnection *)SvIV(SvRV(obj));
  error  = (GdaError *)SvIV(SvRV(err_obj));

  gda_connection_add_single_error(self, error);
}

void get_errors(SV * obj)
{
  Inline_Stack_Vars;
  GdaConnection * self;
  GList *         list;
  long            length;
  long            index;
  GdaError *      data;
  SV *            err_obj_ref;
  SV *            err_obj;

  self   = (GdaConnection *)SvIV(SvRV(obj));

  list   = gda_connection_get_errors(self);
  length = g_list_length(list);

  if (!length) {
    Inline_Stack_Void;
    return;
  }

  Inline_Stack_Reset;

  for(index = 0; index < length; index++) {
    data    = g_list_nth_data(list, index);
    err_obj = objectify_("GDA::Error", data);

    Inline_Stack_Push(err_obj);
  }

  g_list_free(list); /* The errors in the node data are now objects and not freed */

  Inline_Stack_Done;
}


/*********************************************************************
** ATTRIBUTE: Flags                                                 **
*********************************************************************/

long get_flags(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_get_flags(self);
}

void set_flags(SV * obj, long flags)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_set_flags(self, flags);
}


/*********************************************************************
** ATTRIBUTE: Is Open                                               **
*********************************************************************/

long is_open(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_is_open(self);
}


/*********************************************************************
** ATTRIBUTE: Password                                              **
*********************************************************************/

char * password(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_get_password(self);
}


/*********************************************************************
** ATTRIBUTE: Provider                                              **
*********************************************************************/

char * get_provider(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return (char *)gda_connection_get_provider(self);
}

void set_provider(SV * obj, char * provider)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  gda_connection_set_provider(self, provider);
}


/*********************************************************************
** ATTRIBUTE: User                                                  **
*********************************************************************/

char * user(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_get_user(self);
}


/*********************************************************************
** ATTRIBUTE: Version                                               **
*********************************************************************/

char * version(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_get_version(self);
}


/*********************************************************************
** QUERIES                                                          **
*********************************************************************/

long supports(SV * obj, long feature)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_supports(self, feature);
}


/*********************************************************************
** COMMANDS                                                         **
*********************************************************************/

long begin_transaction(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_begin_transaction(self);
}

/* Named 'close_impl' instead of 'close' to avoid name clash */
void close_impl(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  gda_connection_close(self);
}

long commit(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_commit_transaction(self);
}

SV * execute(SV * obj, char * command)
{
  GdaConnection * self;
  GdaRecordset *  result;
  long            count;

  self = (GdaConnection *)SvIV(SvRV(obj));

  result = gda_connection_execute(self, command, &count, 0 /* flags unused for now */);

  /* TODO: What to do about returning record count? What about flags? */

  if (result) {
    return objectify_("GDA::Recordset", result);
  } else {
    return NULL; /* TODO: Is this ok? */
  }
}

/* Named 'open_impl' instead of 'open' to avoid name clash */
long open_impl(SV * obj, char * dsn, char * user, char * password)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_open(self, dsn, user, password);
}

long rollback(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_rollback_transaction(self);
}

void set_default_db(SV * obj, char * dsn)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  gda_connection_set_default_db(obj, dsn);
}

char * sql2xml(SV * obj, char * sql)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_sql2xml(obj, sql);
}

long start_logging(SV * obj, char * filename)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_start_logging(self, filename);
}

long stop_logging(SV * obj)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  gda_connection_stop_logging(self);
}

char * xml2sql(SV * obj, char * xml)
{
  GdaConnection * self;

  self = (GdaConnection *)SvIV(SvRV(obj));

  return gda_connection_xml2sql(obj, xml);
}

/*
** EOF
*/

