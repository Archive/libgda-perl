#!/usr/bin/perl -w
#
# Error.pm
#
# $Revision$
#
# NOTE:
#
#   * gda_error_list_free() is not reflected here, since it is for internal
#     use.
#
# TODO:
#
#   * gda_errors_from_exception()
#   * What about gda_error_signals(), gda_error_class_init() and
#     gda_error_init() from 'strings libgda-client.a'?
#   * Should we really disallow creation of these GDA::Error objects
#     via new()? GDA::Connection has a function for us to add our
#     own errors, but we don't have a way to set the attributes of an
#     error after we create it.
#
# Copyright (C) 2001 Gregor N. Purdy. All rights reserved.
#
# This program is free software. It may be modified and/or
# distributed under the same terms as Perl itself.
#

use strict;

package GDA::Error;

use GDA;

use Inline 'C';

use Inline 'C' => Config =>
  INC          => '-I/usr/include/gda -I/usr/include/glib-1.2 -I/usr/lib/glib/include -I/usr/include/gtk-1.2 -I/usr/include/gnome-xml',
  AUTO_INCLUDE => '#include "gda-error.h"',
  LIBS         => '-lgda-client -lgda-common';
Inline->init; # TODO: Why is this necessary?


1;

=head1 NAME

GDA::Error - GDA error Perl bindings

=head1 SYNOPSIS

  use GDA 'my_app', 'my_ver', 'my_prog';
  use GDA::Error;
  ...

=head1 DESCRIPTION

TODO

=head1 AUTHOR

Gregor N. Purdy E<lt>gregor@focusresearch.comE<gt>

=head1 LICENSE

This program is free software. It may be modified and/or
distributed under the same terms as Perl itself.

=head1 COPYRIGHT

Copyright (C) 2001 Gregor N. Purdy. All rights reserved.

=cut

__DATA__
__C__


/*********************************************************************
** SUPPORT                                                          **
*********************************************************************/

static SV * objectify_(char * class, void * self)
{
  SV * obj_ref;
  SV * obj;

  obj_ref = newSViv(0);
  obj     = newSVrv(obj_ref, class);

  sv_setiv(obj, (IV)self);
  SvREADONLY_on(obj);

  return obj_ref;
}


/*********************************************************************
** CONSTRUCTOR and DESTRUCTOR                                       **
*********************************************************************/

SV * new(char * class)
{
  GdaError * self;

  self = gda_error_new();

  return objectify_(class, self);
}

void DESTROY(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  gda_error_free(self);
}


/*********************************************************************
** ATTRIBUTE: Description                                           **
*********************************************************************/

char * description(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  return (char *)gda_error_description(self);
}


/*********************************************************************
** ATTRIBUTE: Help URL                                              **
*********************************************************************/

char * help_url(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  return (char *)gda_error_helpurl(self);
}


/*********************************************************************
** ATTRIBUTE: Native Message                                        **
*********************************************************************/

char * native_msg(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  return (char *)gda_error_nativeMsg(self);
}


/*********************************************************************
** ATTRIBUTE: Number                                                **
*********************************************************************/

long number(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  return gda_error_number(self);
}


/*********************************************************************
** ATTRIBUTE: Real Command                                          **
*********************************************************************/

char * real_command(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  return (char *)gda_error_realcommand(self);
}


/*********************************************************************
** ATTRIBUTE: Source                                                **
*********************************************************************/

char * source(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  return (char *)gda_error_source(self);
}


/*********************************************************************
** ATTRIBUTE: SQL State                                             **
*********************************************************************/

char * sql_state(SV * obj)
{
  GdaError * self;

  self = (GdaError *)SvIV(SvRV(obj));

  return (char *)gda_error_sqlstate(self);
}


/*
** EOF
*/

