#!/usr/bin/perl -w
#
# Feature.pm
#
# $Revision$
#
# Copyright (C) 2001 Gregor N. Purdy. All rights reserved.
#
# This program is free software. It may be modified and/or
# distributed under the same terms as Perl itself.
# 

use strict;

package GDA::Connection::Feature;

use Inline 'C';

use Inline 'C' => Config =>
  INC          => '-I/usr/include/gda -I/usr/include/glib-1.2 -I/usr/lib/glib/include -I/usr/include/gtk-1.2 -I/usr/include/gnome-xml',
  AUTO_INCLUDE => '#include "gda-config.h"',
  LIBS         => '-lgda-common';

1;

__DATA__
__C__

long FOREIGN_KEYS()
{
  return GDA_Connection_FEATURE_FOREIGN_KEYS,
}

long INHERITANCE()
{
  return GDA_Connection_FEATURE_INHERITANCE,
}

long OBJECT_ID()
{
  return GDA_Connection_FEATURE_OBJECT_ID,
}

long PROCS()
{
  return GDA_Connection_FEATURE_PROCS,
}

long SEQUENCES()
{
  return GDA_Connection_FEATURE_SEQUENCES,
}

long SQL()
{
  return GDA_Connection_FEATURE_SQL,
}

long SQL_SUBSELECT()
{
  return GDA_Connection_FEATURE_SQL_SUBSELECT,
}

long TRANSACTIONS
{
  return GDA_Connection_FEATURE_TRANSACTIONS,
}

long TRIGGERS
{
  return GDA_Connection_FEATURE_TRIGGERS,
}

long VIEWS
{
  return GDA_Connection_FEATURE_VIEWS,
}

long XML_QUERIES
{
  return GDA_Connection_FEATURE_XML_QUERIES
}

