#!/usr/bin/perl -w
#
# Field.pm
#
# $Revision$
#
# NOTE:
#
#   * The only value-access function implemented for now is string-
#     conversion. Perl has a single scalar data type and can take a
#     string value representing a number and treat it correctly (at
#     least with respect to small-enough numbers). We may need to
#     be smarter later, including dealing with Math::BigInt, et al.
#
# TODO:
#
#   * What is the difference between gda_field_get_type() and
#     gda_field_get_typecode()?
#   * What about gda_field_class_init() and gda_field_init()?
#   * what about gda_field_signals()?
#
# Copyright (C) 2001 Gregor N. Purdy. All rights reserved.
#
# This program is free software. It may be modified and/or
# distributed under the same terms as Perl itself.
#

use strict;

package GDA::Field;

use GDA;

use Inline 'C';

use Inline 'C' => Config =>
  INC          => '-I/usr/include/gda -I/usr/include/glib-1.2 -I/usr/lib/glib/include -I/usr/include/gtk-1.2 -I/usr/include/gnome-xml',
  AUTO_INCLUDE => '#include "gda-field.h"',
  LIBS         => '-lgda-client';
Inline->init(); # Why do we have to do this?


1;

=head1 NAME

GDA::Field - GDA error Perl bindings

=head1 SYNOPSIS

  use GDA 'my_app', 'my_ver', 'my_prog';
  use GDA::Field;
  ...

=head1 DESCRIPTION

TODO

=head1 AUTHOR

Gregor N. Purdy E<lt>gregor@focusresearch.comE<gt>

=head1 LICENSE

This program is free software. It may be modified and/or
distributed under the same terms as Perl itself.

=head1 COPYRIGHT

Copyright (C) 2001 Gregor N. Purdy. All rights reserved.

=cut

__DATA__
__C__


/*********************************************************************
** SUPPORT                                                          **
*********************************************************************/

static SV * objectify_(char * class, void * self)
{
  SV * obj_ref;
  SV * obj;

  obj_ref = newSViv(0);
  obj     = newSVrv(obj_ref, class);

  sv_setiv(obj, (IV)self);
  SvREADONLY_on(obj);

  return obj_ref;
}


/*********************************************************************
** CONSTRUCTOR and DESTRUCTOR                                       **
*********************************************************************/

SV * new(char * class)
{
  GdaField * self;

  self = gda_field_new();

  return objectify_(class, self);
}

void DESTROY(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  gda_field_free(self);
}


/*********************************************************************
** ATTRIBUTE: Actual Size                                           **
*********************************************************************/

long actual_size(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_actual_size(self);
}


/*********************************************************************
** ATTRIBUTE: C Type                                                **
*********************************************************************/

long c_type(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_cType(self);
}


/*********************************************************************
** ATTRIBUTE: Defined Size                                          **
*********************************************************************/

long defined_size(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_defined_size(self);
}


/*********************************************************************
** ATTRIBUTE: Is NULL                                               **
*********************************************************************/

long is_null(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_isnull(self);
}


/*********************************************************************
** ATTRIBUTE: Name                                                  **
*********************************************************************/

char * name(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_name(self);
}


/*********************************************************************
** ATTRIBUTE: Native Type                                           **
*********************************************************************/

long native_type(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_nativeType(self);
}


/*********************************************************************
** ATTRIBUTE: Scale                                                 **
*********************************************************************/

long scale(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_scale(self);
}


/*********************************************************************
** ATTRIBUTE: Type                                                  **
*********************************************************************/

long type(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

  return gda_field_type(self);
}


/*********************************************************************
** ATTRIBUTE: Type Code                                             **
*********************************************************************/

long type_code(SV * obj)
{
  GdaField * self;

  self = (GdaField *)SvIV(SvRV(obj));

#define gda_field_typecode(f)  ((f)->real_value->_u.v._d)
  return gda_field_typecode(self);
}


/*********************************************************************
** ATTRIBUTE: Type Name                                             **
*********************************************************************/

#define MAX_TYPE_NAME_LEN 1024
static char type_name_buffer[MAX_TYPE_NAME_LEN + 1];

char * type_name(SV * obj)
{
  GdaField *    self;
  GDA_ValueType type;

  self = (GdaField *)SvIV(SvRV(obj));

  type =  gda_field_typecode(self);
  return gda_fieldtype_2_string(type_name_buffer, MAX_TYPE_NAME_LEN, type);
}


/*********************************************************************
** ATTRIBUTE: Value                                                 **
*********************************************************************/

SV * value(SV * obj)
{
  GdaField * self;
  long       length;
  char *     buffer;
  SV *       result;

  self = (GdaField *)SvIV(SvRV(obj));
  length = gda_field_actual_size(self);

  buffer = malloc(length + 1);

  if (!buffer) {
    return NULL; /* TODO: Is this OK? */
  }

  gda_stringify_value(buffer, length, self);

  result = newSVpv(buffer, 0); /* TODO: Can we copy straight to a SV buffer? */

  free(buffer);

  return result;
}



/*
** EOF
*/

