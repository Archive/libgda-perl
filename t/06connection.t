#!/usr/bin/perl -w
#
# 06connection.t
#
# $Revision: 1.1 $
#
# Copyright (C) 2001 Gregor N. Purdy. All rights reserved.
#
# This program is free software. It may be modified and/or
# distributed under the same terms as Perl itself.
#

use strict;

print "1..3\n";

my $prog = 'libgda-perl-test-06';
use GDA $prog, $GDA::VERSION, $0;
use GDA::Connection;
use GDA::Error;

print "not " if $@;
print "ok 1\n";


#
# Create a DSN we can use:
#

use Cwd;

my $dir           = cwd . "/t/database";
my $dsn_name      = "$prog-dsn";
#my $provider_name = "GNOME_GDA_Provider_Default_ConnectionFactory";
my $provider_name = "GNOME_GDA_Provider_Postgres_ConnectionFactory";
my $provider_id   = "OAFIID:$provider_name";
#my $connect       = "DIRECTORY=$dir";
my $connect       = "HOST=localhost; DATABASE=gregor";
my $user          = 'gregor';
my $password      = '';

print "\n";
print "Testing with the following settings:\n";
print "  Directory     = '$dir'\n";
print "  DSN Name      = '$dsn_name'\n";
print "  Provider Name = '$provider_name'\n";
print "  Provider ID   = '$provider_id'\n";
print "  Connect       = '$connect'\n";
print "  User          = '$user'\n";
print "  Password      = '$password'\n";
print "\n";

print "Checking if directory '$dir' exists...";
if (-d $dir) {
  print "YES\n";
} else {
  print "NO (Making it)\n";
  mkdir $dir, 0770;
}

print "Finding the OAF prefix...";
my $prefix = `oaf-config --prefix`;
chomp $prefix;
print "$prefix\n";

my $oaf = "$prefix/share/oaf/$provider_name.oaf";
print "Looking for the provider's .oaf file...";
if (-e $oaf) {
  print "$oaf\n";
} else {
  print "(NOT FOUND)\n";
}

=no
use GDA::DSN;
my $dsn = new GDA::DSN;
$dsn->name($dsn_name);
$dsn->provider($provider_id);
$dsn->connect($connect);
$dsn->save();
=cut

print "ok 2\n";

#
# Create a table in the database:
#

my %db;

dbmopen %db, "$dir/tla", 0660;

print "\n";
print "The table contains:\n";
foreach my $key (sort keys %db) {
  printf "  %-3s  %s\n", $key, $db{$key};
}
print "\n";

print "Installing standard rows into table...\n";
$db{TLA} = "Three Letter Acronym";
$db{GNU} = "GNU's Not Unix";
$db{LCD} = "Liquid Crystal Display";
$db{CRT} = "Cathode Ray Tube";
dbmclose %db;

#
# Create a Connection to the database:
#

print "Creating a connection...\n";
my $conn = new GDA::Connection;

print "Specifying the provider...\n";
$conn->provider($provider_id);

print "Opening the connection...\n";
my $result = $conn->open($connect, $user, $password);

if ($result != 0) {
  my @errors = $conn->error;
  foreach (@errors) {
    if (ref $_ ne 'GDA::Error') {
      warn "Error object is not of 'GDA::Error' class (it is '"
        . ref($_)
        . "')!!!";
      next;
    }

    print "GDA::Error: '", $_->description, "'\n";
  }

  print "not ";
}

print "ok 3\n";

exit 0;

