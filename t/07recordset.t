#!/usr/bin/perl -w
#
# 07recordset.t
#
# $Revision: 1.1 $
#
# Copyright (C) 2001 Gregor N. Purdy. All rights reserved.
#
# This program is free software. It may be modified and/or
# distributed under the same terms as Perl itself.
#

use strict;

print "1..1\n";

use GDA 'app', 'ver', $0;
use GDA::Connection;
use GDA::Recordset;
use GDA::Error;

my $conn = GDA::Connection->new(
  PROVIDER => 'Postgres',
#  CONNECT  => {
#    HOST     => 'localhost',
#    DATABASE => 'gregor'
#  }
  CONNECT => "HOST=localhost; DATABASE=gregor",
  USER => 'gregor'
);

die "$0: Unable to make GDA::Connection!\n" unless defined($conn);

if (not $conn->is_open) {
  my @errors = $conn->error;
  foreach (@errors) {
    if (ref $_ ne 'GDA::Error') {
      warn "Error object is not of 'GDA::Error' class (it is '"
        . ref($_)
        . "')!!!";
      next;
    }

    print "GDA::Error: '", $_->description, "'\n";
  }

  die "$0: Unable to connect!\n";
}

my $data = $conn->execute("SELECT tablename, tableowner FROM pg_tables");

die "$0: Unable to execute!\n" unless $data;

foreach ($data->all_rows()) {
  print join('|', @$_), "\n";
}

$data->close();
undef $data;

$conn->close();
undef $conn;

print "ok 1";

exit 0;
